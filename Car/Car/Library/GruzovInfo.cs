﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Car.Library
{
    class GruzovInfo : AutoInfo 
    {
        private int _Capacity = 0;

        public int Capacity
        {
            get
            {
                return _Capacity;
            }
            set
            {
                _Capacity = value;
            }
        }
        public GruzovInfo()
        {
            this.CarMaxSpeed = 120;
        }

        public int CarSpeed
        {
            get
            {
                return base.CarMaxSpeed;
            }
            set
            {
                if (value < 120)
                {
                    base.CarMaxSpeed = value;
                };
            }
        }
    }
}
