﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Car.Library
{
    class MotoMotoInfo : MotoInfo 
    {
        private string _MotoType = "";
        public string MotoType
        {
            get
            {
                return _MotoType;
            }
            set
            {
                _MotoType = value;
            }
        }
    }
}
