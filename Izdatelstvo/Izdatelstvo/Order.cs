﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Izdatelstvo
{
    class Order
    {
        private Customer _CustomerID = new Customer();
        private Author _AuthorID = new Author();
        private Distributors _DistributorsID = new Distributors();
        private PrintHouse _PrintHouseID = new PrintHouse();
        private long _Contract = 0;
        private int _Edition = 0;

        public Customer CustomerID
        {
            get
            {
                return _CustomerID;
            }
            set
            {
                if (value != null)
                {
                    _CustomerID = value;
                }
            }
        }
        public Author AuthorID
        {
            get
            {
                return _AuthorID;
            }
            set
            {
                if (value != null)
                {
                    _AuthorID = value;
                }
            }
        }
        public Distributors DistributorsID
        {
            get
            {
                return _DistributorsID;
            }
            set
            {
                if (value != null)
                {
                    _DistributorsID = value;
                }
            }
        }
        public PrintHouse PrintHouseID
        {
            get
            {
                return _PrintHouseID;
            }
            set
            {
                if (value != null)
                {
                    _PrintHouseID = value;
                }
            }
        }
        public long Contract
        {
            get
            {
                return _Contract;
            }
            set
            {
                string C = value.ToString();
                if (value != null)
                {
                    if (C.Length == 13)
                    {
                    _Contract = value;
                    }
                }
            }
        }
        public int Edition
        {
            get
            {
                return _Edition;
            }
            set
            {
                
                if (value >= 0)
                {                   
                        _Contract = value;
                    
                }
            }
        }
    }
}
