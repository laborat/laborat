﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Izdatelstvo
{
    class Author : Person 
    {
        private string _Addition = "";
        private Publication _Book = new Publication();

        public string Addition
        {
            get
            {
                return _Addition;
            }
            set
            {
                if (value != null)
                {
                    _Addition = value;
                }
            }
        }
        public Publication Book
        {
            get
            {
                return _Book;
            }
            set
            {
                if (value != null)
                {
                    _Book = value;
                }
            }
        }
        public virtual void CreatPublication (Publication Name)   
        {
            Console.WriteLine("Я " + this.Name + " создал издание, которое называется " + Name.Name );
            
        }
    }
}
