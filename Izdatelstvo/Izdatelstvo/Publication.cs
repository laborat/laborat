﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Izdatelstvo
{
    class Publication
    {
        private string _Name = "";
        private int _MaxPageNomber = 0;
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                if (value != null)
                {
                    _Name = value;
                }
            }
        }
        public int MaxPageNomber
        {
            get
            {
                return _MaxPageNomber;
            }
            set
            {
                if (value != null)
                {
                    _MaxPageNomber = value;
                }
            }
        }
      
        
        public virtual void write(Author _nameauthor)
        {
            Console.WriteLine("Я " + _nameauthor.Name + " написал " + Name);
        
        }
    }
}
